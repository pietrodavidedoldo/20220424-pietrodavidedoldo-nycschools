//
//  Score.swift
//  NYCSchoolsChallenge
//
//  Created by Pietro Davide Doldo on 4/23/22.
//

import Foundation

struct Score: Decodable {
    let numberOfSatTestTakers: String
    let satReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String
    
    enum CodingKeys: String, CodingKey {
        case numberOfSatTestTakers = "num_of_sat_test_takers"
        case satReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }

}
