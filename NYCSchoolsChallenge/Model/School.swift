//
//  School.swift
//  NYCSchoolsChallenge
//
//  Created by Pietro Davide Doldo on 4/23/22.
//

import Foundation

struct School: Codable {
    let dbn: String
    let schoolName: String
    let boro: String
    let overview: String
    let ellPrograms: String?
    let languageClasses: String?
    let neighborhood: String?
    let campusName: String?
    let location: String?
    let phoneNumber: String?
    let schoolEmail: String?
    let website: String?
    let totalStudents: String?
    let address: String?
    let city: String?
    let zip: String?
    let stateCode: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case boro
        case overview = "overview_paragraph"
        case ellPrograms = "ell_programs"
        case languageClasses = "language_classes"
        case neighborhood
        case campusName = "campus_name"
        case location
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website
        case totalStudents = "total_students"
        case address = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        
        
    }
    
}
