//
//  NetworkManager.swift
//  NYCSchoolsChallenge
//
//  Created by Pietro Davide Doldo on 4/23/22.
//

import Foundation

protocol SchoolService {
    func getSchools(completion: @escaping(Result<[School], NetworkError>)-> Void)
    func getScores(for dbn: String, completion: @escaping(Result<[Score], NetworkError>)-> Void)
}

class NetworkManager: SchoolService {
    
    static let shared = NetworkManager()
    
    private init() {}

    func getSchools(completion: @escaping (Result<[School], NetworkError>)-> Void) {
        // Ideally, with more time, the endpoint and the decodable model can be astracted so that I don't have to initialize
        // the type of endpoint here. Similarly, I won't have to put the Decodable Object to convert inside the method's signature,
        // but I would use a generic and probably infer the associated type from the endpoint used.
        let endpoint = SchoolEndpoint.getSchools
        // setting the request info from the specified endpoint
        // URL
        let url = URL(string: endpoint.url)!
        var urlRequest = URLRequest(url: url)
        // HTTP Method
        urlRequest.httpMethod = endpoint.httpMethod.rawValue
        // Request Headers
        endpoint.headers?.forEach({ header in
            urlRequest.setValue(header.value as? String, forHTTPHeaderField: header.key)
        })
        // in the closure, [unowned self] should be used is self is going to be called inside the closure
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let networkError = NetworkError(data: data, response: response, error: error) {
                completion(.failure(networkError))
            }
                    
            do {
                let schools = try JSONDecoder().decode([School].self, from: data!)
                completion(.success(schools))
            } catch {
                completion(.failure(.decodingError(error)))
            }

        }
        task.resume()
    }

    func getScores(for dbn: String, completion: @escaping(Result<[Score], NetworkError>)-> Void) {
        let endpoint = SchoolEndpoint.getSchoolScore(dbn: dbn)
        // URL
        let url = URL(string: endpoint.url)!
        var urlRequest = URLRequest(url: url)
        // HTTP Method
        urlRequest.httpMethod = endpoint.httpMethod.rawValue
        // Request Headers
        endpoint.headers?.forEach({ header in
            urlRequest.setValue(header.value as? String, forHTTPHeaderField: header.key)
        })
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let networkError = NetworkError(data: data, response: response, error: error) {
                completion(.failure(networkError))
            }
                    
            do {
                let scores = try JSONDecoder().decode([Score].self, from: data!)
                completion(.success(scores))
            } catch {
                completion(.failure(.decodingError(error)))
            }
        }
        task.resume()
    }


}
