//
//  SchoolEndpoints.swift
//  NYCSchoolsChallenge
//
//  Created by Pietro Davide Doldo on 4/23/22.
//

import Foundation

enum SchoolEndpoint: Endpoint {
    
    case getSchools
    case getSchoolScore(dbn: String)
    
    
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var baseURLString: String {
        return "https://data.cityofnewyork.us/resource"
    }
    
    var path: String {
        switch self {
        case let .getSchoolScore(dbn):
            return "/f9bf-2cp4.json?dbn=\(dbn)"
        case .getSchools:
            return "/s3k6-pzi2.json"
        }
    }
    
    var headers: [String : Any]? {
        switch self {
        case let .getSchoolScore(dbn):
            return ["dbn": "\(dbn)"]
        default: return nil
        }
    }
    
    var body: [String : Any]? {
        return nil
    }
}

