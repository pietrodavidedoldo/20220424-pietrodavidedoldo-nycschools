//
//  ViewController.swift
//  NYCSchoolsChallenge
//
//  Created by Pietro Davide Doldo on 4/23/22.
//

import UIKit

class SchoolsListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: SchoolsListViewModel? {
        didSet {
            fillUI()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "SchoolTableViewCell", bundle: nil), forCellReuseIdentifier: "SchoolTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        fillUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "TEST"
        if let navVC = self.navigationController {
            navVC.navigationBar.topItem?.title = "NYC Schools"
        }
    }
    
    private func fillUI() {
        if !isViewLoaded {
            return
        }
        guard let viewModel = viewModel else { return }
        
        viewModel.getSchools { result in
            switch result {
                case .failure(_):
                    // here we should probably display an error in a humanized way
                    // leave this as a placeholder, so that if I have time I will make a better error handling directly
                    // from the viewModel, so that the controller can just show a clear and understandable error message for the user
                    DispatchQueue.main.async { [weak self] in
                        let alert = UIAlertController(title: nil, message: "An error occurred while fetching the data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: .default))
                        self?.show(alert, sender: nil)
                    }
                case .success(_):
                    
                    /*
                     If schools data are correctly extracted, then I want to go back to the main queue to perform UI changes,
                     since I was coming from a background queue where I was performing network operations.
                     
                     The use of weak is important here, because my request can terminate maybe when my view is not more displayed.
                     For example, there can be other buttons that redirects the user to a different page, when the request is still in progress.
                     At that point, calling self.tableView would cause a crash because self would be nil
                     */
                    DispatchQueue.main.async { [weak self] in
                        self?.tableView.reloadData()
                    }
                }
            }
    }
    
    fileprivate func createSchoolCell(for indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCell", for: indexPath) as? SchoolTableViewCell else {
            return UITableViewCell()
        }
        if let viewModel = viewModel {
            cell.fillUI(with: viewModel.schools[indexPath.row])
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSchoolDetail" {
            if let destinationVC = segue.destination as? SchoolDetailViewController {
                destinationVC.viewModel = SchoolDetailViewModelFromScratch(school: self.viewModel!.selectedSchool!)
            }
        }
    }

}

extension SchoolsListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let vm = viewModel else { return 0 }
        return vm.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return createSchoolCell(for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel?.didSelectSchool(at: indexPath.row)
        self.performSegue(withIdentifier: "showSchoolDetail", sender: self)
    }
    
}

