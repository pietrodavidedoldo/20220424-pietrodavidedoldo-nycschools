//
//  SchoolDetailViewController.swift
//  NYCSchoolsChallenge
//
//  Created by Pietro Davide Doldo on 4/23/22.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    
    @IBOutlet weak var satTestTakersLabel: UILabel!
    
    @IBOutlet weak var satReadingScoreLabel: UILabel!
    
    @IBOutlet weak var satMathScoreLabel: UILabel!
    
    @IBOutlet weak var satWritingScoreLabel: UILabel!
    
    @IBOutlet weak var overviewText: UITextView!
    
    var viewModel: SchoolDetailViewModel? {
        didSet {
            fillUI()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fillUI()
    }
    
    private func fillUI() {
        if !isViewLoaded {
            return
        }
        guard let viewModel = viewModel else { return }
        clearLabels()
        if let navVC = self.navigationController {
            navVC.navigationBar.topItem?.title = viewModel.school.schoolName
        }
        viewModel.getSchoolScores { result in
            
            switch result {
            case .failure(_):
                // here we should probably display an error in a humanized way
                // leave this as a placeholder, so that if I have time I will make a better error handling directly
                // from the viewModel, so that the controller can just show a clear and understandable error message for the user
                DispatchQueue.main.async { [weak self] in
                    self?.clearLabels()
                    let alert = UIAlertController(title: nil, message: "An error occurred while fetching the data", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .default))
                    self?.show(alert, sender: nil)
                }
            case let .success(scores):
                DispatchQueue.main.async { [weak self] in
                    guard !scores.isEmpty else { return }
                    self?.satTestTakersLabel.text = scores.first!.numberOfSatTestTakers
                    self?.satReadingScoreLabel.text = scores.first!.satReadingAvgScore
                    self?.satMathScoreLabel.text = scores.first!.satMathAvgScore
                    self?.satWritingScoreLabel.text = scores.first!.satWritingAvgScore
                    self?.overviewText.text = viewModel.school.overview
                }
                
            }
        }
    }
    
    private func clearLabels() {
        satTestTakersLabel.text = ""
        satReadingScoreLabel.text = ""
        satMathScoreLabel.text = ""
        satWritingScoreLabel.text = ""
        overviewText.text = ""
    }

}
