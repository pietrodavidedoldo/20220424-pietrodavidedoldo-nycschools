//
//  SchoolTableViewCell.swift
//  NYCSchoolsChallenge
//
//  Created by Pietro Davide Doldo on 4/23/22.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    //
    /*
     In this case, since we only have 2 labels to populate, it wouldn't make sense to have a viewModel.
     Its only function would be to give us the fullAddress already formatted.
     Another option would be to use the viewModel of the SchoolsListViewController, since it is really just about formatting the address.
     If more data manipulation was required, then this SchoolTableViewCell would have its own view model class.
     */
    func fillUI(with school: School) {
        self.schoolNameLabel.text = school.schoolName
        self.schoolAddressLabel.text = formatFullAddressString(from: school)
    }
    
    private func formatFullAddressString(from school: School) -> String {
        let address = school.address != nil ? school.address! : ""
        let city = school.city != nil ? school.city! : ""
        let zip = school.zip != nil ? school.zip! : ""
        let state = school.stateCode != nil ? school.stateCode! : ""
        let fullAddress = address  + "\n" + city + ", " + zip + ", " + state
        
        return fullAddress
    }
    
}
