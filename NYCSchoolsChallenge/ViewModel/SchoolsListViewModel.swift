//
//  SchoolsListViewModel.swift
//  NYCSchoolsChallenge
//
//  Created by Pietro Davide Doldo on 4/23/22.
//

import Foundation


protocol SchoolsListViewModel {
    var schools: [School] { get }
    var selectedSchool: School? { get set }
    func getSchools(completion: @escaping(Result<[School], NetworkError>)-> Void)
    func didSelectSchool(at row: Int)
}

class SchoolsListViewModelFromScratch : SchoolsListViewModel {
    var schools: [School] = []
    var selectedSchool: School?
    
    func getSchools(completion: @escaping(Result<[School], NetworkError>)-> Void) {
        NetworkManager.shared.getSchools { [unowned self] result in
            switch result {
            case .failure(_): self.schools = []
            case let .success(schools): self.schools = schools
            }
            completion(result)
        }
    }
    
    func didSelectSchool(at row: Int) {
        guard row < schools.count else { return }
        self.selectedSchool = schools[row]
    }
    
    
}
