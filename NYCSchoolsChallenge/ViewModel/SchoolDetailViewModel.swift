//
//  SchoolDetailViewModel.swift
//  NYCSchoolsChallenge
//
//  Created by Pietro Davide Doldo on 4/23/22.
//

import Foundation

protocol SchoolDetailViewModel {
//    var scores: Score? { get set }
    var dbn: String { get set }
    var school: School { get set }
    func getSchoolScores(completion: @escaping(Result<[Score], NetworkError>)-> Void)
}


class SchoolDetailViewModelFromScratch: SchoolDetailViewModel {
    var dbn: String
    var school: School
    
    init(school: School){
        self.school = school
        self.dbn = school.dbn
    }
    func getSchoolScores(completion: @escaping(Result<[Score], NetworkError>) -> Void) {
        NetworkManager.shared.getScores(for: dbn) { result in
            completion(result)
        }
    }

    
    
}
